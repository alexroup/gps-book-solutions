import unittest
import numpy as np
import math as math
import random as rand

import utility_functions as uf


class testCase_wrap(unittest.TestCase):

    def test_wrap_lower_0000(self):
        for ll in np.arange(-10.0, 10.0, 1.0):
            for x in np.arange(-10.0, 10.0, 1.0):
                assert uf.wrap(x, ll, ll+1.0) == ll, 'incorrect wrap at lower limit'

    def test_wrap_lower_0001(self):
        for ll in np.arange(-10.0, 10.0, 2.0):
            for x in np.arange(-10.0, 10.0, 2.0):
                assert uf.wrap(x, ll, ll+2.0) == ll, 'incorrect wrap at lower limit'

    def test_wrap_mid_0000(self):
        for ll in np.arange(-10.0, 10.0, 1.0):
            for x in np.arange(-10.5, 10.5, 1.0):
                assert uf.wrap(x, ll, ll+1.0) == ll + 0.5, 'incorrect wrap between limits'

    def test_wrap_mid_0001(self):
        for ll in np.arange(-10.0, 10.0, 2.0):
            for x in np.arange(-9.5, 10.5, 2.0):
                assert uf.wrap(x, ll, ll+2.0) == ll + 0.5, 'incorrect wrap between limits'

    def test_wrap_upper_0000(self):
        for ul in np.arange(-10.0, 10.0, 1.0):
            for x in np.arange(-10.0, 10.0, 1.0):
                assert uf.wrap(x, ul-1.0, ul) == ul-1.0, 'incorrect wrap at upper limit'


testSuite_wrap = unittest.makeSuite(testCase_wrap,'test')


class testCase_geodetic_to_ECEF_spherical(unittest.TestCase):

    def setUp(self):
        self.Re = 6371000.0 # meters

        self.lla0 = np.matrix([
            [0.0, 0.0,       0.0,     0.0,         math.pi/2, -math.pi/2,  0.0 ],
            [0.0, math.pi/2, math.pi, -math.pi/2,  0.0,        0.0,        0.0 ],
            [0.0, 0.0,       0.0,     0.0,         0.0,        0.0,       -self.Re]
            ])

        self.pos0 = np.matrix([
            [self.Re,  0.0,     -self.Re,  0.0,      0.0,      0.0,      0.0],
            [0.0,      self.Re,  0.0,     -self.Re,  0.0,      0.0,      0.0],
            [0.0,      0.0,      0.0,      0.0,      self.Re, -self.Re,  0.0]
            ])

    def test_geodetic_to_ECEF_spherical_equatorial_0000(self):

        dShape = self.lla0.shape
        for n in range(dShape[1]):
            pos = uf.geodetic_to_ECEF_spherical(self.lla0[:,n])
            np.testing.assert_array_almost_equal(pos, self.pos0[:,n], 6)

    def test_geodetic_to_ECEF_spherical_equatorial_0001(self):

        dShape = self.lla0.shape
        for n in range(dShape[1]):
            lla = uf.ECEF_to_geodetic_spherical(self.pos0[:,n])
            np.testing.assert_array_almost_equal(lla, self.lla0[:,n], 6)

    def test_geodetic_to_ECEF_spherical_equatorial_0002(self):

        for n in range(1000):
            pos0 = np.matrix([[rand.gauss(0.0, self.Re)],[rand.gauss(0.0, self.Re)],[rand.gauss(0.0, self.Re)]])

            lla = uf.ECEF_to_geodetic_spherical(pos0)
            pos = uf.geodetic_to_ECEF_spherical(lla)

            np.testing.assert_array_almost_equal(pos, pos0, 6)


testSuite_testCase_geodetic_to_ECEF_spherical = unittest.makeSuite(testCase_geodetic_to_ECEF_spherical,'test')


runner = unittest.TextTestRunner()
runner.run(testSuite_wrap)
runner.run(testSuite_testCase_geodetic_to_ECEF_spherical)
