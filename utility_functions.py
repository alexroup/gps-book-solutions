# # Imports

import math
import numpy as np
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy import cos, sin, arctan, sqrt, arctan2, hypot


# # Constants

r2d = 180./math.pi
d2r = math.pi/180.
Re = 6371000. # meters
c = 299792458. # m/s
a = 6378137.0 # m
finv = 298.257223563
omega_e = 7292115.0e-11
e2 = (2*finv - 1)/finv**2
ep2 = 1/finv*(2-1/finv)/(1-1/finv)**2
b = a*(1-1/finv)

mu = 3.986005e14
Ohmdot = 7.2921151467e-5

# # Classes

class IterationLimitExceeded(Exception):
    """Exception raised when number of iteration exceeds its limit.

    Attributes:
        nIter -- number of iterations
    """
    def __init__(self, nIter):
        self.nIter = nIter
    def __str__(self):
        return repr(self.nIter)

# # Functions

def wrap(x, lb, ub):
    return (x-lb)%(ub-lb) + lb


def geodetic_to_ECEF_spherical(lla):

    phi = lla[0,0]
    lam = lla[1,0]
    h   = lla[2,0]

    X = (Re+h)*cos(phi)*cos(lam)
    Y = (Re+h)*cos(phi)*sin(lam)
    Z = (Re+h)*sin(phi)

    return np.matrix([[X],[Y],[Z]])


def ECEF_to_geodetic_spherical(xyz):
    X = xyz[0,0]
    Y = xyz[1,0]
    Z = xyz[2,0]

    p = hypot(X, Y)
    phi = arctan2(Z,p)
    lam = arctan2(Y,X)
    h   = sqrt(X*X + Y*Y + Z*Z) - Re

    return np.matrix([[phi],[lam],[h]])


## CNE - ECEF to NED transform matrix
def getCNE(phi, lam):
    sL = sin(lam)
    cL = cos(lam)
    sP = sin(phi)
    cP = cos(phi)

    return np.matrix([[-cL*sP, -sL*sP, cP],
                     [-sL, cL, 0],
                     [-cL*cP, -sL*cP, -sP]])


## CEN - NED to ECEF transform matrix
def getCEN(phi,lam):
    return np.transpose(getCNE(phi,lam))


## ECEF to ENU transform matrix
def getEcefToEnu(phi, lam):
    sL = sin(lam)
    cL = cos(lam)
    sP = sin(phi)
    cP = cos(phi)

    return np.matrix([[-sL, cL, 0],
                     [-sP*cL, -sP*sL, cP],
                     [cP*cL, cP*sL, sP]])


## ECEF to ENU transform matrix
def getEnuToEcef(phi, lam):
    return np.transpose(getEcefToEnu(phi,lam))


def getN(phi):
    return a/sqrt(1-e2*math.sin(phi)**2)


def ECEF_to_geodetic_WGS84_iterative(X,Y,Z):

    tol = 1e-6

    # longitude
    lam = arctan2(Y,X)

    # horizontal radius
    p = hypot(X,Y)

    # initial guess at latitude from spherical
    phi0 = arctan2(Z,p)

    phi_last = phi0-math.pi/2.
    phi = phi0
    h_last = -Re
    h = 0

    niter = 0

    while (fabs(phi-phi_last) > tol/Re) or (fabs(h-h_last) > tol) :

        N = a/sqrt(1-e2*sin(phi)**2)
        h_last = h
        if fabs(phi) < pi/2:
            h = p/cos(phi) - N
        else:
            h = Z/sin(phi) - N*(1-e2)

        phi_last = phi
        phi = arctan2(Z, p*(1-e2 * N/(N+h)))
        niter += 1

        if (niter > iterMax):
            raise(IterationLimitExceeded(niter))

    return (phi, lam, h, niter)

# Function [lat, lon, alt] = wgsxyz2lla(xyz) transforms the
# 3 x 1 vector xyz representing a point in WGS84 xyz coordinates
# into its corresponding latitude (radians), longitude (radians),
# and height above WGS84 ellipsoid (in meters)
#
# Adapted from Wgsxyz2lla.m by Andrew Barrows

def wgsxyz2lla(xyz):

    #  This dual-variable iteration seems to be 7 or 8 times faster than
    #  a one-variable (in latitude only) iteration.  AKB 7/17/95

    A_EARTH = 6378137
    flattening = 1/298.257223563
    NAV_E2 = (2-flattening)*flattening # also e^2
    rad2deg = 180/math.pi

    if ((xyz[0] == 0.0) & (xyz[1] == 0.0)):
        wlon = 0.0
    else:
        wlon = arctan2(xyz[1], xyz[0])

    if ((xyz[0] == 0.0) & (xyz[1] == 0.0) & (xyz[2] == 0.0)):
        error('WGS xyz at center of earth')
    else:
        # Make initial lat and alt guesses based on spherical earth.
        rhosqrd = xyz[0]*xyz[0] + xyz[1]*xyz[1]
        rho = sqrt(rhosqrd)
        templat = arctan2(xyz[2], rho)
        tempalt = sqrt(rhosqrd + xyz[2]*xyz[2]) - A_EARTH
        rhoerror = 1000.0
        zerror   = 1000.0

        #  Newton's method iteration on templat and tempalt makes
        #   the residuals on rho and z progressively smaller.  Loop
        #   is implemented as a 'while' instead of a 'do' to simplify
        #   porting to MATLAB

        while ((abs(rhoerror) > 1e-6) | (abs(zerror) > 1e-6)):
            slat = sin(templat)
            clat = cos(templat)
            q = 1 - NAV_E2*slat*slat
            r_n = A_EARTH/sqrt(q)
            drdl = r_n*NAV_E2*slat*clat/q # d(r_n)/d(latitutde)

            rhoerror = (r_n + tempalt)*clat - rho
            zerror   = (r_n*(1 - NAV_E2) + tempalt)*slat - xyz[2]

            #                 --                               -- --      --
            #                 |  drhoerror/dlat  drhoerror/dalt | |  a  b  |
            # Find Jacobian   |                     |=|        |
            #                 |   dzerror/dlat    dzerror/dalt  | |  c  d  |
            #                 --                               -- --      --

            aa = drdl*clat - (r_n + tempalt)*slat
            bb = clat
            cc = (1 - NAV_E2)*(drdl*slat + r_n*clat)
            dd = slat

            #  Apply correction = inv(Jacobian)*errorvector

            invdet = 1.0/(aa*dd - bb*cc)
            templat = templat - invdet*(+dd*rhoerror -bb*zerror)
            tempalt = tempalt - invdet*(-cc*rhoerror +aa*zerror)

        wlat = templat
        walt = tempalt

    return (wlat, wlon, walt)

def geodetic_to_ECEF_WGS84(phi, lam, h):

    N = getN(phi)

    X = (N + h)*math.cos(phi)*math.cos(lam)
    Y = (N + h)*math.cos(phi)*sin(lam)
    Z = (N*(1-e2) + h)*sin(phi)

    return (X, Y, Z)


# seems to require more operations than the iterative method
def ECEF_to_geodetic_WGS84_heikkinen(X,Y,Z):

    r = sqrt(X*X + Y*Y)
    E2 = a*a - b*b
    F = 54.0*b*b*Z*Z
    G = r*r + (1.0-e2)*Z*Z - e2*E2
    C = (e2*e2*F*r*r)/(G**3)
    S = (1.0 + C + sqrt(C*C + 2.0*C))**(1.0/3.0)
    P = F/(3.0*(S + 1.0/S + 1.0)**2 * G*G)
    Q = sqrt(1.0+2.0*e2*e2 *P)
    r_0 = -(P*e2*r)/(1.0+Q) + sqrt(0.5 * a*a*(1.0+1.0/Q) - (P*(1-e2)*Z*Z)/(Q*(1.0+Q)) - 0.5*P*r*r)
    U = sqrt((r-e2*r_0)**2 + Z*Z)
    V = sqrt((r-e2*r_0)**2 + (1-e2)*Z*Z)
    Z_0 = (b*b*Z)/(a*V)
    h = U*(1.0- b*b/(a*V))
    phi = arctan((Z + ep2*Z_0)/r)
    lam = arctan2(Y, X)

    return (phi, lam, h)


# frame rotation about the input frame 1st axis
# argument theta is the rotation of output frame w.r.t. input frame
def R1(theta):
    return np.matrix([[1,  0,           0        ],
                      [0,  cos(theta), sin(theta)],
                      [0, -sin(theta), cos(theta)]])


# frame rotation about the shared Y axis
# argument theta is the rotation of output frame w.r.t. input frame
def R2(theta):
    return np.matrix([[cos(theta), 0, -sin(theta)],
                      [0,          1,  0         ],
                      [sin(theta), 0,  cos(theta)]])


# frame rotation about the shared Z axis
# argument theta is the rotation of output frame w.r.t. input frame
def R3(theta):
    return np.matrix([[ cos(theta), sin(theta), 0],
                      [-sin(theta), cos(theta), 0],
                      [ 0,          0,          1]])

# Nav to Body attitude DCM, a 3-2-1 aerospace Euler rotation sequence
def getCBN(phi, theta, psi):
    return R1(phi)*R2(theta)*R3(psi)


# Body to Nav attitude DCM, a 1-2-3 aerospace Euler rotation sequence
def getCNB(phi, theta, psi):
    return R3(-psi)*R2(-theta)*R1(-phi)


# inertial to body axis 3rd axis direction cosine multiplied by vectors stored as row vectors
def R3_rowVecRot(a, x):

    col0 = cos(a)*x[:,0] + sin(a)*x[:,1]
    col1 = -sin(a)*x[:,0] + cos(a)*x[:,1]
    col2 = x[:,2]

    return np.concatenate(col0, col1, col2)


# inertial to body axis 3rd axis direction cosine rate multiplied by vectors stored as row vectors
def R3dot_rowVecRot(a, w, x):

    col0 = -sin(a)*w*x[:,0] + cos(a)*w*x[:,1]
    col1 = -cos(a)*w*x[:,0] - sin(a)*w*x[:,1]
    col2 = zeros(size(x,1),1)

    return np.concatenate(col0, col1, col2)


# POSKEP - geocentric equatorial position from orbital elements
#
# Syntax:  pos = poskep(a,e,i,Ohm,omega,v)
#
# Inputs:
#    a - semi-major axis
#    e - eccentricity
#    i - inclination
#    Ohm - longitude of the ascending node
#    omega - argument of the periapsis
#    v - true anomaly
def poskep(a,e,i,Ohm,omega,v):

    rorb = a*(1-e*e)/(1+e*cos(v))*np.matrix([[cos(v)][sin(v)][0]])

    # rotate the orbital plane into geocentric equatorial
    return R3(-Ohm)*R1(-i)*R3(-omega)*rorb


#SATPOS - satellite terrestrial positions from time and ephemeris
# Reference IS-GPS-200D, Table 20-IV, pp. 97-98
#
# Syntax:  pos_T, vel_T, tbias = satpvt(t, eph)
#
# Inputs:
#    t - Time vector, length n
#    eph - ephemeris struct
#    Note: t and eph are expected to have matching dimensions
#
# Outputs:
#    pos_T - nx3 array of terrestrial position row vectors
#    vel_T - nx3 array of terrestrial velocity row vectors
#    tbias - nx1 array of time bias expressed in meters


##

def satpvt(t, eph):

    from scipy.optimize import root

    ##

    nsat = len(t)  # number of satellites

    n0 = sqrt(mu)*np.ones(nsat,1)/(eph['sqrta']**3.0)

    tk = wrap(t-eph['toe'],-302400,302400)

    n = n0 + eph['dn']  # mean orbital rate
    a = eph['sqrta']**2 # semimajor axis

    Mk = eph.m0 + n*tk

    def f_obj(x):
        return x - eph['e']*sin(x) - Mk

#    options = optimoptions('fsolve')
#    options.Display='off'
#    [Ek,fval,exitflag,output] = fsolve(@(x) x - eph.e*sin(x) - Mk, zeros(nsat,1), options)

    sol = root(f_obj, np.zeros((nsat,1)))

    try:
        assert max(abs(sol.fun))<1e-4
        assert sol.success
    except:
        print(sol.message, '\n\n')
        raise

    vk = arctan2(sqrt(1-eph['e']**2.0) * sin(Ek)/(1-eph['e']*cos(Ek)), (cos(Ek)-eph['e'])/(1-eph['e']*cos(Ek)))

    Phik = wrap(vk + eph['w'],-pi,pi)
    duk = eph['cus']*sin(2*Phik) + eph['cuc']*cos(2*Phik)
    drk = eph['crs']*sin(2*Phik) + eph['crc']*cos(2*Phik)
    dik = eph['cis']*sin(2*Phik) + eph['cic']*cos(2*Phik)

    uk = wrap(Phik + duk,-pi,pi)
    rk = a*(1-eph['e']*cos(Ek)) + drk
    ik = eph['i0'] + dik + eph['idot']*tk

    # rotation from vernal equinox (inertial) to terrestrial
    # for velocity frame rotation
    theta = wrap(Ohmdot*(tk + eph['toe']),-pi,pi)

    # longitude of ascending node (terrestrial)
    Ohmk = wrap(eph['omg0'] + eph['odot']*tk - theta, -pi,pi)

    ## position

    # orbital plane position
    xkp = rk*cos(uk)
    ykp = rk*sin(uk)

    # convert orbital plane position to terrestrial
    xk = xkp*cos(Ohmk) - ykp*cos(ik)*sin(Ohmk)
    yk = xkp*sin(Ohmk) + ykp*cos(ik)*cos(Ohmk)
    zk = ykp*sin(ik)

    pos_T = [xk, yk, zk]  # terrestrial position
    pos_I = C3rvecrot(-theta,pos_T)

    ## velocity

    # orbital plane velocity
    vel_o = ((n*a/(1-eph.e*cos(Ek)))*np.ones((1,3))) * [-sin(Ek), sqrt(1-eph.e**2.0)*cos(Ek), np.zeros((nsat,1))]

    # rotate orbital velocity about argument of periapsis
    xdotkp = vel_o[:,1]*cos(eph.w) - vel_o[:,2]*sin(eph.w)
    ydotkp = vel_o[:,1]*sin(eph.w) + vel_o[:,2]*cos(eph.w)

    # inertial velocity
    vel_I[:,1] = xdotkp*cos(eph.omg0) - ydotkp*cos(ik)*sin(eph.omg0)
    vel_I[:,2] = xdotkp*sin(eph.omg0) + ydotkp*cos(ik)*cos(eph.omg0)
    vel_I[:,3] = ydotkp*sin(ik)

    # terrestrial velocity
    vel_T = C3dotrvecrot(theta, (eph.odot - Ohmdot), pos_I) + C3rvecrot(theta, vel_I)

    ## time bias

    F = -2*sqrt(mu)/c^2 # -4.442807633e-10
    dtr = F*eph.e*eph.sqrta*sin(Ek)

    tbias = c*(eph.af0 + eph.af1*(t-eph.toc) + eph.af2*(t-eph.toc)**2.0 + dtr)  # in meters

    return (pos_T, vel_T, tbias)


